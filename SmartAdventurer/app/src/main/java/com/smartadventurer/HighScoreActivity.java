package com.smartadventurer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.smartadventurer.adapter.HighScoreAdapter;
import com.smartadventurer.models.HighScore;
import com.smartadventurer.rest.RestClient;

import org.apache.http.Header;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HighScoreActivity extends AppCompatActivity {
    private ListView listView;
    private List<HighScore> highScoreList;
    private ProgressBar pbLoader;
    private HighScoreAdapter highScoreAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_high_score);
        listView = (ListView) findViewById(R.id.listView);
        listView.setVisibility(View.GONE);
        pbLoader = (ProgressBar) findViewById(R.id.pbLoader);

        getHighScoreList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void getHighScoreList() {
        RestClient.get("scores/highest.json", null,
                new JsonHttpResponseHandler() {
                    @Override
                    public void onStart() {
                        super.onStart();
                        pbLoader.setVisibility(View.VISIBLE);
                        if (highScoreList == null) {
                            highScoreList = new ArrayList<HighScore>();
                        }
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        pbLoader.setVisibility(View.GONE);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.i("response", response.toString());
                        highScoreList = HighScore.getListFromJson(response);
                        if (!highScoreList.isEmpty()) {
                            highScoreAdapter = new HighScoreAdapter(HighScoreActivity.this, highScoreList);
                            listView.setAdapter(highScoreAdapter);
                            listView.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Log.e("response", "error code : " + statusCode);
                    }
                });
    }
}
