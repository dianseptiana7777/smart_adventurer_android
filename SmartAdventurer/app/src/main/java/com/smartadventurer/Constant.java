package com.smartadventurer;

public class Constant {
    public final static int REQUEST_CODE_GAME = 1;

    //value game
    public final static int SCORE_CORRECT = 100;
    public final static int COST_USE_FEATURE_NEXT_NODE = 100;

    //waktu untuk menjawab pertanyaan
    public final static int TIME_EASY = 180;
    public final static int TIME_MEDIUM = 120;
    public final static int TIME_HARD = 60;
    public final static int TIME_VERYHARD = 30;

    public static final String KEY_GAME_ID = "game_id";

    public final static int MAX_DISTANCE = 10;
}
