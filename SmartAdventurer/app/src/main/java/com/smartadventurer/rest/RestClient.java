package com.smartadventurer.rest;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class RestClient {
    private static final String BASE_URL = "http://smartadventurer.herokuapp.com/api/v1/";
    private static AsyncHttpClient client = new AsyncHttpClient();

    private static String getApiUrl(String relativeUrl) {
        Log.d("Smart Adventurer", "URL Request : " + BASE_URL + relativeUrl);
        return BASE_URL + relativeUrl;
    }

    public static void get(String url, RequestParams params,
                           AsyncHttpResponseHandler responseHandler) {
        client.get(getApiUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params,
                            AsyncHttpResponseHandler responseHandler) {
        client.post(getApiUrl(url), params, responseHandler);
    }

    public static void put(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.put(getApiUrl(url), params,
                responseHandler);
    }

    public static void delete(String url,
                              AsyncHttpResponseHandler responseHandler) {
        client.delete(getApiUrl(url), responseHandler);
    }
}
