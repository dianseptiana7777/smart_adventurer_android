package com.smartadventurer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.smartadventurer.R;

import java.util.List;

public class NavDrawerAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private List<String> menuList;

    public NavDrawerAdapter(Context context, List<String> menuList) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.menuList = menuList;
    }

    @Override
    public int getCount() {
        return menuList.size();
    }

    @Override
    public String getItem(int position) {
        return menuList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listitem_drawer, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvMenu = (TextView) convertView.findViewById(R.id.tvMenu);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        String menu = getItem(position);
        viewHolder.tvMenu.setText(menu);
        return convertView;
    }

    class ViewHolder {
        TextView tvMenu;
    }
}