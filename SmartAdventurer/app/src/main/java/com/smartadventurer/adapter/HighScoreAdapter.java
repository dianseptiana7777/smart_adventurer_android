package com.smartadventurer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.smartadventurer.R;
import com.smartadventurer.models.HighScore;

import java.util.List;

public class HighScoreAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private List<HighScore> highScoreList;

    public HighScoreAdapter(Context context, List<HighScore> highScoreList) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.highScoreList = highScoreList;
    }

    @Override
    public int getCount() {
        return highScoreList.size();
    }

    @Override
    public HighScore getItem(int position) {
        return highScoreList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listitem_highscores, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvNickname = (TextView) convertView.findViewById(R.id.tvNickname);
            viewHolder.tvTotalScores = (TextView) convertView.findViewById(R.id.tvTotalScores);
            viewHolder.tvTotalSeconds = (TextView) convertView.findViewById(R.id.tvTotalSeconds);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        HighScore highScore = getItem(position);
        viewHolder.tvNickname.setText(highScore.getNickname());
        viewHolder.tvTotalScores.setText(Integer.toString(highScore.getTotal_scores()));
        viewHolder.tvTotalSeconds.setText(Integer.toString(highScore.getTotal_seconds()));
        return convertView;
    }

    class ViewHolder {
        TextView tvNickname;
        TextView tvTotalScores;
        TextView tvTotalSeconds;
    }
}