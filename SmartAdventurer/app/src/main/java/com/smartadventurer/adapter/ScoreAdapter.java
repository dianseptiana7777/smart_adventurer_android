package com.smartadventurer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.smartadventurer.R;
import com.smartadventurer.models.Score;

import java.util.List;

public class ScoreAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private List<Score> scoreList;

    public ScoreAdapter(Context context, List<Score> scoreList) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.scoreList = scoreList;
    }

    @Override
    public int getCount() {
        return scoreList.size();
    }

    @Override
    public Score getItem(int position) {
        return scoreList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listitem_scores, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvGameName = (TextView) convertView.findViewById(R.id.tvGameName);
            viewHolder.tvScore = (TextView) convertView.findViewById(R.id.tvScore);
            viewHolder.tvSeconds = (TextView) convertView.findViewById(R.id.tvSeconds);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Score score = getItem(position);
        viewHolder.tvGameName.setText(score.getName());
        viewHolder.tvScore.setText(Integer.toString(score.getScore()));
        viewHolder.tvSeconds.setText(Integer.toString(score.getSeconds()));
        return convertView;
    }

    class ViewHolder {
        TextView tvGameName;
        TextView tvScore;
        TextView tvSeconds;
    }
}