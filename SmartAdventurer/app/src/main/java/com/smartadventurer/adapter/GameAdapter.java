package com.smartadventurer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.smartadventurer.R;
import com.smartadventurer.models.Game;

import java.util.ArrayList;
import java.util.List;

public class GameAdapter extends BaseAdapter implements Filterable {
    private Context context;
    private LayoutInflater inflater;
    private List<Game> gameList;
    private List<Game> originGameList;
    private Filter gameFilter;

    public GameAdapter(Context context, List<Game> gameList) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.gameList = gameList;
        this.originGameList = gameList;
    }

    @Override
    public int getCount() {
        return gameList.size();
    }

    @Override
    public Game getItem(int position) {
        return gameList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listitem_game, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvGameName = (TextView) convertView.findViewById(R.id.tvGameName);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Game game = getItem(position);
        viewHolder.tvGameName.setText(game.getName());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (gameFilter == null)
            gameFilter = new GameFilter();
        return gameFilter;
    }

    class ViewHolder {
        TextView tvGameName;
    }

    private class GameFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            if (constraint == null || constraint.length() == 0) {
                results.values = originGameList;
                results.count = originGameList.size();
            } else {
                List<Game> games = new ArrayList<Game>();

                for (int i = 0; i < originGameList.size(); i++) {
                    Game game = originGameList.get(i);
                    if (game.getName().toUpperCase()
                            .contains(constraint.toString().toUpperCase())) {
                        games.add(game);
                    }

                }
                results.values = games;
                results.count = games.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            if (results.count == 0)
                notifyDataSetInvalidated();
            else {
                gameList = (List<Game>) results.values;
                notifyDataSetChanged();
            }

        }
    }

    public void resetData() {
        gameList = originGameList;
    }
}