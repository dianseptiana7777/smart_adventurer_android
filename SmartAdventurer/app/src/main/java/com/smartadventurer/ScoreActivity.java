package com.smartadventurer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.smartadventurer.adapter.ScoreAdapter;
import com.smartadventurer.models.Score;
import com.smartadventurer.preferences.DataPreferences;
import com.smartadventurer.rest.RestClient;

import org.apache.http.Header;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ScoreActivity extends AppCompatActivity {
    private ListView listView;
    private List<Score> scoreList;
    private ProgressBar pbLoader;
    private ScoreAdapter scoreAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_score);
        listView = (ListView) findViewById(R.id.listView);
        listView.setVisibility(View.GONE);
        pbLoader = (ProgressBar) findViewById(R.id.pbLoader);

        getScoreList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void getScoreList() {
        int user_id = new DataPreferences(ScoreActivity.this).getUserSession().getUser_id();
        RestClient.get("players/" + user_id + "/scores.json", null,
                new JsonHttpResponseHandler() {
                    @Override
                    public void onStart() {
                        super.onStart();
                        pbLoader.setVisibility(View.VISIBLE);
                        if (scoreList == null) {
                            scoreList = new ArrayList<>();
                        }
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        pbLoader.setVisibility(View.GONE);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.i("response", response.toString());
                        scoreList = Score.getListFromJson(response);
                        if (!scoreList.isEmpty()) {
                            scoreAdapter = new ScoreAdapter(ScoreActivity.this, scoreList);
                            listView.setAdapter(scoreAdapter);
                            listView.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Log.e("response", "error code : " + statusCode);
                    }
                });
    }
}
