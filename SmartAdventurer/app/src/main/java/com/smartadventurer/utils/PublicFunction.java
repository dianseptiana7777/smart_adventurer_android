package com.smartadventurer.utils;

import com.smartadventurer.Constant;

import java.util.List;

public class PublicFunction {
    public static int getTimer(String difficulty_level) {
        if (difficulty_level.equalsIgnoreCase("easy_question"))
            return Constant.TIME_EASY * 1000;
        else if (difficulty_level.equalsIgnoreCase("normal_question"))
            return Constant.TIME_MEDIUM * 1000;
        else if (difficulty_level.equalsIgnoreCase("hard_question"))
            return Constant.TIME_HARD * 1000;
        else if (difficulty_level.equalsIgnoreCase("very_hard_question"))
            return Constant.TIME_VERYHARD * 1000;
        else
            return 0;
    }

    public static String convertToCommaSeparated(List<String> strings) {
        StringBuffer sb = new StringBuffer("");
        int size = strings.size();
        for (int i = 0; strings != null && i < size; i++) {
            sb.append(strings.get(i));
            if (i < size - 1) {
                sb.append(',');
            }
        }
        return sb.toString();
    }
}
