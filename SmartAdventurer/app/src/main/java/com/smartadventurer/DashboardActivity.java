package com.smartadventurer;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.smartadventurer.adapter.NavDrawerAdapter;
import com.smartadventurer.models.Edge;
import com.smartadventurer.models.Game;
import com.smartadventurer.models.Graph;
import com.smartadventurer.models.Node;
import com.smartadventurer.models.Question;
import com.smartadventurer.models.User;
import com.smartadventurer.models.Vertex;
import com.smartadventurer.preferences.DataPreferences;
import com.smartadventurer.rest.RestClient;
import com.smartadventurer.utils.DijkstraAlgorithm;
import com.smartadventurer.utils.GPSUtils;
import com.smartadventurer.utils.JsonUtil;
import com.smartadventurer.utils.PublicFunction;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class DashboardActivity extends AppCompatActivity implements
        GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {

    private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 3; // in Meters
    private static final long MINIMUM_TIME_BETWEEN_UPDATES = 5 * 1000; // in Milliseconds

    protected LocationManager locationManager;

    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private NavDrawerAdapter adapter;
    private GoogleMap mMap;
    private GPSUtils gps;
    private LinearLayout llGameInfo, llDistance;
    private List<Marker> nodeMarkerList = null;
    private List<Integer> nodeIdList = null;
    //lokasi user
    private LatLng userLocation;
    Location prevLocation = new Location("Previous User Location");
    private Marker userMarker = null;
    //text info di halaman permainan
    private TextView tvTimer, tvScore, tvHealth, tvDistance, tvWalkDistance;
    //menu di halaman permainan
    private ImageView settingFocus, myLocation, periksa, nextMode;
    private int setFocus = 0;
    //variable games
    private Game game;
    private int game_id = 0;
    private int score = 0;
    private int health = 3;
    private int total_time = 0;
    private int totalWalkDistance = 0;
    private List<String> questionIdsList;
    private Node currentNode, finishNode;
    private Marker currentNodeMarker = null;
    private boolean isChooseNextNode = false;
    private long startTime = 0L;
    private long timeInMilliseconds = 0L;
    private long timeSwapBuff = 0L;
    private long updatedTime = 0L;
    private Handler customHandler = new Handler();
    private Runnable updateTimerThread = new Runnable() {

        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            updatedTime = timeSwapBuff + timeInMilliseconds;
            int secs = (int) (updatedTime / 1000);
            total_time = secs;
            tvTimer.setText(Integer.toString(total_time));
            customHandler.postDelayed(this, 0);
        }
    };

    private List<Vertex> nodes;
    private List<Edge> edges;
    private int nextModeUsed = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        bindView();

        addDrawerItems();
        setupDrawer();

        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        gps = new GPSUtils(DashboardActivity.this);
        setUpMapIfNeeded();

        settingFocus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!nodeMarkerList.isEmpty()) {
                    if (game_id == 0) {
                        Toast.makeText(DashboardActivity.this,
                                "Fitur in iakan bekerja apabila anda telah memilih game untuk dimainkan.",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        if (setFocus == 0) {
                            goToMyLocation();
                        } else if (setFocus == 1) {
                            LatLng myNodeLocation = new LatLng(currentNode.getLatitude(), currentNode.getLongitude());
                            CameraPosition cameraPosition = new CameraPosition.Builder()
                                    .target(myNodeLocation).zoom(17).build();
                            mMap.animateCamera(CameraUpdateFactory
                                    .newCameraPosition(cameraPosition));
                            setFocus = 2;
                        } else if (setFocus == 2) {
                            currentNodeMarker = nodeMarkerList.get(0);
                            LatLngBounds.Builder builder = new LatLngBounds.Builder();
                            for (Marker marker : nodeMarkerList) {
                                if (marker != null) {
                                    builder.include(marker.getPosition());
                                }
                            }
                            if (userLocation != null)
                                builder.include(userLocation);
                            LatLngBounds bounds = builder.build();
                            int padding = 120;
                            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                            mMap.animateCamera(cameraUpdate);
                            setFocus = 0;
                        }
                    }
                } else {
                    goToMyLocation();
                }
            }
        });

        myLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMyLocation();
            }
        });

        periksa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (game_id != 0) {
                    if (!isChooseNextNode) {
                        int distance = getDistance();
//                        if (distance > 0) { // >  0 : untuk menghiraukan jarak 10 m
                        if (distance <= Constant.MAX_DISTANCE) { // <= Constant.MAX_DISTANCE untuk menggunakan jarak
                            if (currentNode.getNode_type().equalsIgnoreCase("start")) {
                                Toast.makeText(getApplicationContext(), "Anda telah berhasil mencapai titik start, silahkan pilih rute selanjutnya dengan klik salah satu marker", Toast.LENGTH_SHORT).show();
                                showNextNode();
                            } else if (currentNode.getNode_type().equalsIgnoreCase("trap")) {
                                showTrapDialog();
                            } else if (currentNode.getNode_type().equalsIgnoreCase("finish")) {
                                showFinishDialog();
                            } else if (currentNode.getNode_type().contains("question")) {
                                showFoundQuestionDialog();
                            }
                        } else {
                            Toast.makeText(DashboardActivity.this,
                                    "Silakan menuju ke lokasi yang telah anda pilih.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(DashboardActivity.this,
                                "Silakan pilih rute selanjutnya.",
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(DashboardActivity.this,
                            "Fitur in iakan bekerja apabila anda telah memilih game untuk dimainkan.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        nextMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (game_id != 0) {
                    if (currentNode != null && nextModeUsed <= 3) {
                        if ((score - ((nextModeUsed + 1) * Constant.COST_USE_FEATURE_NEXT_NODE)) >= 0) {
                            nextModeUsed++;
                            score = score - (nextModeUsed * Constant.COST_USE_FEATURE_NEXT_NODE);
                            tvScore.setText(Integer.toString(score));

                            Graph graph = new Graph(nodes, edges);
                            DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
                            dijkstra.execute(Vertex.getVertexById("Node_" + Integer.toString(currentNode.getId()), nodes));
                            LinkedList<Vertex> path = dijkstra.getPath(Vertex.getVertexById("Node_" + Integer.toString(finishNode.getId()), nodes));

                            if (path.size() > 0) {
                                int i = 0;
                                Double totalWeight = 0.0;

                                for (Vertex vertex : path) {
                                    Log.i("Langkah Dijkstra", vertex.toString());
                                    i++;
                                    if (i != path.size()) {
                                        Edge edge = graph.getEdge(vertex, path.get(i));
                                        totalWeight += edge.getWeight();
                                    }
                                }

                                Toast.makeText(DashboardActivity.this,
                                        "Titik terbaik selanjutnya : \n" + path.get(1).toString(),
                                        Toast.LENGTH_SHORT).show();
                                Log.i("Total bobot minimum", String.valueOf(totalWeight));
                                Log.i("Titik terbaik selanjutnya", path.get(1).toString());
                            } else {
                                Toast.makeText(DashboardActivity.this,
                                        "Path Kosong.",
                                        Toast.LENGTH_SHORT).show();
                                System.out.println("Path kosong");
                            }
                        } else {
                            Toast.makeText(DashboardActivity.this,
                                    "Score anda tidak cukup untuk menggunakan fitur ini.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(DashboardActivity.this,
                                "Anda telah menggunakan fitur ini sebanyak 3x.",
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(DashboardActivity.this,
                            "Fitur ini akan bekerja apabila anda telah memilih game untuk dimainkan.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        resetValue();

        //cek ada nickname & user_id atau tidak
        isHasNicknameAndId();
//        goToMyLocation();
        startLocationService();
    }

    private int getDistance() {
        if (currentNode != null && userLocation != null) {
            Location start = new Location("Tujuan");
            start.setLatitude(currentNode.getLatitude());
            start.setLongitude(currentNode.getLongitude());
            Location current = new Location("Lokasiku");
            current.setLatitude(userLocation.latitude);
            current.setLongitude(userLocation.longitude);
            return (int) start.distanceTo(current);
        } else return 0;
    }

    private int getWalkDistance(Location newUserLocation){
        prevLocation.setLatitude(userLocation.latitude);
        prevLocation.setLongitude(userLocation.longitude);
        int walkDistance = (int) prevLocation.distanceTo(newUserLocation);
        return walkDistance;
    }

    private void bindView() {
        mDrawerList = (ListView) findViewById(R.id.navList);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        myLocation = (ImageView) findViewById(R.id.myLocation);
        myLocation.bringToFront();
        settingFocus = (ImageView) findViewById(R.id.settingFocus);
        settingFocus.bringToFront();
        periksa = (ImageView) findViewById(R.id.periksa);
        periksa.bringToFront();
        nextMode = (ImageView) findViewById(R.id.nextMode);
        nextMode.bringToFront();
        nextMode.setVisibility(View.GONE);
        llGameInfo = (LinearLayout) findViewById(R.id.llGameInfo);
        llDistance = (LinearLayout) findViewById(R.id.llDistance);

        tvTimer = (TextView) findViewById(R.id.tvTimer);
        tvScore = (TextView) findViewById(R.id.tvCurrentScore);
        tvHealth = (TextView) findViewById(R.id.tvHealth);
        tvDistance = (TextView) findViewById(R.id.tvDistance);
        tvWalkDistance = (TextView) findViewById(R.id.tvWalkDistance);
    }

    private void addDrawerItems() {
        List<String> menuList = new ArrayList<String>();
        menuList.add("Pilih Game");
        menuList.add("Cara Bermain");
        menuList.add("Skor");
        menuList.add("Skor Tertinggi");
        menuList.add("About Us");
        adapter = new NavDrawerAdapter(this, menuList);
        mDrawerList.setAdapter(adapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openPage(position);
            }
        });
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
        }
        return false;
    }

    //membuka halaman berdasarkan posisi menu drawer
    private void openPage(int position) {
        switch (position) {
            case 0:
                Intent intent1 = new Intent(DashboardActivity.this, GameListActivity.class);
                startActivityForResult(intent1, Constant.REQUEST_CODE_GAME);
                break;
            case 1:
                Intent intent2 = new Intent(DashboardActivity.this, HowToPlayActivity.class);
                startActivity(intent2);
                break;
            case 2:
                Intent intent3 = new Intent(DashboardActivity.this, ScoreActivity.class);
                startActivity(intent3);
                break;
            case 3:
                Intent intent4 = new Intent(DashboardActivity.this, HighScoreActivity.class);
                startActivity(intent4);
                break;
            case 4:
                Intent intent5 = new Intent(DashboardActivity.this, AboutActivity.class);
                startActivity(intent5);
                break;
            default:
                break;
        }
        mDrawerLayout.closeDrawers();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constant.REQUEST_CODE_GAME) {
            if (resultCode == RESULT_OK) {
                setUpMap();
                resetValue();
                game_id = data.getIntExtra(Constant.KEY_GAME_ID, 0);
                getGameById(game_id);
            } else if (resultCode == RESULT_CANCELED) {
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p>
     * If it isn't installed {@link com.google.android.gms.maps.SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            mMap.setOnMarkerClickListener(this);
            mMap.setOnInfoWindowClickListener(this);
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            } else {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        resetLayout();
        goToMyLocation();
    }

    public void resetLayout() {
        setFocus = 0;
        mMap.clear();
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        mMap.setMyLocationEnabled(true);
//        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
    }

    private void resetValue() {
        health = 3;
        tvHealth.setText(Integer.toString(health));
        score = 0;
        tvScore.setText(Integer.toString(score));
        total_time = 0;
        tvTimer.setText(Integer.toString(total_time));
        tvDistance.setText("0");
        nextModeUsed = 0;

        game = null;
        currentNode = null;
        finishNode = null;
        game_id = 0;
        questionIdsList = new ArrayList<String>();
        nodeIdList = new ArrayList<Integer>();
        nodeMarkerList = new ArrayList<>();
        startTime = 0L;
        timeInMilliseconds = 0L;
        timeSwapBuff = 0L;
        updatedTime = 0L;
        customHandler = new Handler();

        totalWalkDistance = 0;
        tvWalkDistance.setText("0");
    }

    private void goToMyLocation() {
//        if (mMap.getMyLocation() != null )
//            userLocation = new LatLng(mMap.getMyLocation().getLatitude(), mMap.getMyLocation().getLongitude());
//        else
            userLocation = new LatLng(gps.getLatitude(), gps.getLongitude());

        if (userMarker != null) {
            userMarker.remove();
        }
        userMarker = mMap.addMarker(new MarkerOptions().position(
                userLocation).title("Lokasiku").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.ic_marker_user)));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(userLocation).zoom(17).build();
        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));
        setFocus = 1;
    }

    private void getGameById(int game_id) {
        RestClient.get("games/" + game_id + ".json", null,
                new JsonHttpResponseHandler() {
                    ProgressDialog progressView;

                    @Override
                    public void onStart() {
                        super.onStart();
                        progressView = new ProgressDialog(DashboardActivity.this);
                        progressView.setIndeterminate(true);
                        progressView.setCancelable(false);
                        progressView.setMessage("Mohon Tunggu");
                        progressView.show();

                        if (game == null) {
                            game = new Game();
                        }

                        nodes = new ArrayList<Vertex>();
                        edges = new ArrayList<Edge>();
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progressView.dismiss();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
//                        Log.i("response", response.toString());
                        game = Game.fromJsonObject(response);
                        if (game != null) {
                            getSupportActionBar().setTitle("Game : " + game.getName());
                            int size = game.getNodes().size();

                            for (Node node : game.getNodes()) {
                                nodeIdList.add(node.getId());
                                if (node.getNode_type().equalsIgnoreCase("finish")) {
                                    finishNode = node;
                                }
                            }

                            nodes = game.getVertexes();
                            edges = game.getEdges();
                            int sizeEdge = edges.size();
                            for (int i = 0; i < sizeEdge; i++) {
                                Edge edge = edges.get(i);
                                Vertex sourceVertex = Vertex.getVertexById("Node_" + edge.getSource_id(), nodes);
                                Vertex destinationVertex = Vertex.getVertexById("Node_" + edge.getDestination_id(), nodes);
                                edge.setSource(sourceVertex);
                                edge.setDestination(destinationVertex);

                                Log.i("Garis", edge.getId() + ", " + edge.getWeight());
                            }

                            //initial untuk start node
                            if (size > 0) {
                                LatLng latLng = new LatLng(game.getNodes().get(0).getLatitude(), game.getNodes().get(0).getLongitude());
                                nodeMarkerList = new ArrayList<Marker>();
                                Marker tempMarker = mMap.addMarker(new MarkerOptions()
                                        .position(latLng)
                                        .title("START")
                                        .icon(BitmapDescriptorFactory
                                                .fromResource(R.drawable.ic_marker_flag_on)));

                                nodeMarkerList.add(tempMarker);
                                currentNode = game.getNodes().get(0);
                                showInstructionStart();
                            } else {
                                Toast.makeText(getApplicationContext(), "Terjadi kesalahan pada game yang Anda pilih", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Log.e("response", "error code : " + statusCode);
                    }
                });
    }

    private void startLocationService() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                MINIMUM_TIME_BETWEEN_UPDATES,
                MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                new MyLocationListener()
        );
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        if (isChooseNextNode == true) {
            int size = nodeMarkerList.size();
            int indexFounded = -1;
            for (int i = 0; i < size; i++) {
                if (nodeMarkerList.get(i).equals(marker)) {
                    indexFounded = nodeIdList.indexOf(currentNode.getConnected_nodes().get(i).getId());
                }
            }
            if (indexFounded > -1) {
                isChooseNextNode = false;
                nextMode.setVisibility(View.GONE);
                removeAllNodeMarker();
                Node tempNode = game.getNodes().get(indexFounded);
                if (tempNode != null) {
                    currentNode = tempNode;
                    Toast.makeText(getApplicationContext(), "Silahkan menuju marker berikutnya", Toast.LENGTH_SHORT).show();
                    LatLng latLng = new LatLng(currentNode.getLatitude(), currentNode.getLongitude());
                    nodeMarkerList = new ArrayList<>();
                    Marker tempMarker = mMap.addMarker(new MarkerOptions()
                            .position(latLng)
                            .title("Node " + currentNode.getId())
                            .icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.ic_marker_flag_on)));

                    nodeMarkerList.add(tempMarker);
                    currentNodeMarker = tempMarker;
                    updateDistanceInfo();
                    setFocus = 2;
                    settingFocus.performClick();
                }
            }
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    private void showNextNode() {
        if (currentNode.getConnected_nodes() != null) {
            removeAllNodeMarker();
            nodeMarkerList = new ArrayList<>();

            for (Node node : currentNode.getConnected_nodes()) {
                LatLng tempLatLng = new LatLng(node.getLatitude(), node.getLongitude());
                Marker tempMarker = mMap.addMarker(new MarkerOptions()
                        .position(tempLatLng)
                        .title("Node " + node.getId())
                        .icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.ic_marker_flag_off)));
                nodeMarkerList.add(tempMarker);
            }
            isChooseNextNode = true;
            nextMode.setVisibility(View.VISIBLE);
            setFocus = 2;
            settingFocus.performClick();
            updateDistanceInfo();
        }
    }


    private void removeAllNodeMarker() {
        for (Marker marker : nodeMarkerList) {
            if (marker != null)
                marker.remove();
        }
        nodeMarkerList.clear();
        nodeMarkerList = new ArrayList<>();
        if (currentNodeMarker != null)
            currentNodeMarker.remove();
    }

    /*
    show dialog
     */

    public void showInstructionStart() {
        startTime = SystemClock.uptimeMillis();
        customHandler.postDelayed(updateTimerThread, 0);

        setFocus = 2;
        settingFocus.performClick();
        LayoutInflater layoutInflater = LayoutInflater.from(DashboardActivity.this);
        View view = layoutInflater.inflate(R.layout.dialog_start, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DashboardActivity.this);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (alertDialog.isShowing())
                    alertDialog.dismiss();
            }
        }, 5000);
        updateDistanceInfo();
    }

    public void showFoundQuestionDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(DashboardActivity.this);
        View view = layoutInflater.inflate(R.layout.dialog_found_question, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DashboardActivity.this);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        getQuestion(currentNode.getDifficulty_level(), PublicFunction.convertToCommaSeparated(questionIdsList));
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public void showFinishDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(DashboardActivity.this);
        View view = layoutInflater.inflate(R.layout.dialog_finish, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DashboardActivity.this);
        alertDialogBuilder.setView(view);
        final TextView tvFinishScore = (TextView) view.findViewById(R.id.tvFinishScore);
        final TextView tvFinishTotalTime = (TextView) view.findViewById(R.id.tvFinishTotalTime);
        tvFinishScore.setText("Skor : " + Integer.toString(score));
        tvFinishTotalTime.setText("Waktu : " + Integer.toString(total_time));
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        saveScore();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public void showFailedDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(DashboardActivity.this);
        View view = layoutInflater.inflate(R.layout.dialog_failed, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DashboardActivity.this);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        resetLayout();
                        resetValue();
                        goToMyLocation();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
        timeSwapBuff += timeInMilliseconds;
        customHandler.removeCallbacks(updateTimerThread);
//        resetLayout();
//        resetValue();
//        goToMyLocation();
    }

    public void showTrapDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(DashboardActivity.this);
        View view = layoutInflater.inflate(R.layout.dialog_trap, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DashboardActivity.this);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        resetLayout();
                        resetValue();
                        goToMyLocation();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
        timeSwapBuff += timeInMilliseconds;
        customHandler.removeCallbacks(updateTimerThread);
//        resetLayout();
//        resetValue();
//        goToMyLocation();
    }

    /*
    question
     */
    private void getQuestion(String difficulty_level, String question_ids) {
        RestClient.get("questions/random_by_difficulty.json?difficulty_level=" + difficulty_level + "&question_ids=" + question_ids, null,
                new JsonHttpResponseHandler() {
                    ProgressDialog progressView;

                    @Override
                    public void onStart() {
                        super.onStart();
                        progressView = new ProgressDialog(DashboardActivity.this);
                        progressView.setIndeterminate(true);
                        progressView.setCancelable(false);
                        progressView.setMessage("Memuat pertanyaan");
                        progressView.show();
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progressView.dismiss();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.i("response", response.toString());
                        Question question = Question.fromJsonObject(response);
                        if (question != null) showQuestionDialog(question);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Log.e("response", "error code : " + statusCode);
                    }
                });
    }

    public void showQuestionDialog(final Question question) {
        LayoutInflater layoutInflater = LayoutInflater.from(DashboardActivity.this);
        View view = layoutInflater.inflate(R.layout.dialog_question, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DashboardActivity.this);
        alertDialogBuilder.setView(view);
        final TextView tvQuestion = (TextView) view.findViewById(R.id.tvQuestion);
        final RadioGroup rgAnswer = (RadioGroup) view.findViewById(R.id.rgAnswer);
        final RadioButton rbAnswerA = (RadioButton) view.findViewById(R.id.rbAnswerA);
        final RadioButton rbAnswerB = (RadioButton) view.findViewById(R.id.rbAnswerB);
        final RadioButton rbAnswerC = (RadioButton) view.findViewById(R.id.rbAnswerC);
        final RadioButton rbAnswerD = (RadioButton) view.findViewById(R.id.rbAnswerD);
        final RadioButton rbAnswerE = (RadioButton) view.findViewById(R.id.rbAnswerE);
        final TextView tvQuestionTimer = (TextView) view.findViewById(R.id.tvQuestionTimer);

        tvQuestion.setText(question.getQuestion_text());
        rbAnswerA.setChecked(true);
        rbAnswerA.setText(question.getAnswer_a());
        rbAnswerB.setText(question.getAnswer_b());
        rbAnswerC.setText(question.getAnswer_c());
        rbAnswerD.setText(question.getAnswer_d());
        rbAnswerE.setText(question.getAnswer_e());

        CountDownTimer countDownTimer = null;

        final String[] arrAnswer = {"A", "B", "C", "D", "E"};
        final boolean[] isAnswered = {false};
        final CountDownTimer finalCountDownTimer = countDownTimer;
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("JAWAB", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        int index = rgAnswer.indexOfChild(rgAnswer.findViewById(rgAnswer.getCheckedRadioButtonId()));
                        if (index > -1 && !isAnswered[0]) {
                            isAnswered[0] = true;
                            if (finalCountDownTimer != null) finalCountDownTimer.cancel();

                            if (arrAnswer[index].equalsIgnoreCase(question.getCorrect_answer())) {
                                //correct answer
                                dialog.cancel();
                                Toast.makeText(DashboardActivity.this,
                                        "Jawaban anda benar, silakan pilih rute selanjutnya",
                                        Toast.LENGTH_SHORT).show();

                                score = score + Constant.SCORE_CORRECT;
                                tvScore.setText(Integer.toString(score));

                                questionIdsList.add(Integer.toString(question.getId()));
                                showNextNode();
                            } else {
                                //wrong answer
                                dialog.cancel();
                                Toast.makeText(DashboardActivity.this,
                                        "Maaf jawaban anda salah. Health anda dikurangi",
                                        Toast.LENGTH_SHORT).show();

                                health = health > 0 ? health - 1 : 0;
                                tvHealth.setText(Integer.toString(health));
                                //if (health > 0)
                                //   getQuestion(currentNode.getDifficulty_level(), PublicFunction.convertToCommaSeparated(questionIdsList));
                                //else
                                if (health == 0)
                                    showFailedDialog();
                            }
                        }
                    }
                });
        final AlertDialog alert = alertDialogBuilder.create();
        alert.show();


        final int milisTime = PublicFunction.getTimer(question.getDifficulty_level());
        if (milisTime > 0) {
            tvQuestionTimer.setText(Integer.toString(milisTime / 1000));
            countDownTimer = new CountDownTimer(milisTime, 1000) {

                public void onTick(long millisUntilFinished) {
                    int newTime = (int) (millisUntilFinished / 1000);
                    tvQuestionTimer.setText(Integer.toString(newTime));
                }

                public void onFinish() {
                    tvQuestionTimer.setText("0");
                    int index = rgAnswer.indexOfChild(rgAnswer.findViewById(rgAnswer.getCheckedRadioButtonId()));
                    if (index > -1 && !isAnswered[0]) {
                        if (arrAnswer[index].equalsIgnoreCase(question.getCorrect_answer())) {
                            //correct answer
                            alert.dismiss();
                            Toast.makeText(DashboardActivity.this,
                                    "Jawaban anda benar, silakan pilih rute selanjutnya",
                                    Toast.LENGTH_SHORT).show();

                            score = score + Constant.SCORE_CORRECT;
                            tvScore.setText(Integer.toString(score));

                            questionIdsList.add(Integer.toString(question.getId()));
                            showNextNode();
                        } else {
                            //wrong answer
                            alert.dismiss();
                            Toast.makeText(DashboardActivity.this,
                                    "Maaf jawaban anda salah. Health anda dikurangi",
                                    Toast.LENGTH_SHORT).show();

                            health = health > 0 ? health - 1 : 0;
                            tvHealth.setText(Integer.toString(health));
                            //if (health > 0)
                            //   getQuestion(currentNode.getDifficulty_level(), PublicFunction.convertToCommaSeparated(questionIdsList));
                            //else
                            if (health == 0)
                                showFailedDialog();
                        }
                    }
                }
            }.start();
        } else {
            tvQuestionTimer.setText("");
        }

    }

    /*
    nickname
     */
    private void isHasNicknameAndId() {
        User user = new DataPreferences(DashboardActivity.this).getUserSession();
        if (user.getNickname().equals("") || user.getUser_id() == 0) {
            showInputNicknameDialog();
        } else {
            String nickname = new DataPreferences(DashboardActivity.this).getUserSession().getNickname();
            Toast.makeText(getApplicationContext(), "Selamat datang " + nickname, Toast.LENGTH_SHORT).show();
        }
    }

    private void showInputNicknameDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(DashboardActivity.this);
        View view = layoutInflater.inflate(R.layout.dialog_input_nickname, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DashboardActivity.this);
        alertDialogBuilder.setView(view);
        final EditText etNickname = (EditText) view.findViewById(R.id.etNickname);
        etNickname.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String result = s.toString().replaceAll(" ", "");
                if (!s.toString().equals(result)) {
                    etNickname.setText(result);
                    etNickname.setSelection(result.length());
                }
            }
        });

        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String nickname = etNickname.getText().toString();
                        if (nickname.length() < 4) {
                            Toast.makeText(getApplicationContext(), "nickname harus lebih dari 3 karakter", Toast.LENGTH_SHORT).show();
                            showInputNicknameDialog();
                        } else {
                            dialog.cancel();
                            saveNickNameToServer(nickname);
                        }
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void saveNickNameToServer(final String nickname) {
        RequestParams params = new RequestParams();
        params.put("player[nickname]", nickname);
        RestClient.post("players.json", params,
                new JsonHttpResponseHandler() {
                    ProgressDialog progressView;

                    @Override
                    public void onStart() {
                        super.onStart();
                        progressView = new ProgressDialog(DashboardActivity.this);
                        progressView.setIndeterminate(true);
                        progressView.setCancelable(false);
                        progressView.setMessage("Mohon Tunggu, Nickname Anda sedang disimpan");
                        progressView.show();
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progressView.dismiss();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e("response", response.toString());

                        if (response.has("errors")) {
                            JSONObject jsonObject = JsonUtil.getJSONObject(response, "errors");
                            if (jsonObject.has("nickname")) {
                                JSONArray jsonArray = JsonUtil.getJSONArray(jsonObject, "nickname");

                                try {
                                    String message = jsonArray.get(0).toString();
                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                } catch (JSONException e) {
                                }
                            }
                            progressView.dismiss();
                            showInputNicknameDialog();
                        } else {
                            User user = User.fromJsonObject(response);
                            DataPreferences dataPreferences = new DataPreferences(DashboardActivity.this);
                            dataPreferences.createUserSession(user);
                            Toast.makeText(getApplicationContext(), "Selamat datang " + nickname, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Log.e("response", "error code : " + statusCode);
//                       if(errorResponse!=null){
//                        if (errorResponse.has("errors")) {
//                            JSONObject jsonObject = JsonUtil.getJSONObject(errorResponse, "errors");
//                            if (jsonObject.has("nickname")) {
//                                JSONArray jsonArray = JsonUtil.getJSONArray(jsonObject, "nickname");
//
//                                try {
//                                    String message = jsonArray.get(0).toString();
//                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
//                                } catch (JSONException e) {
//                                }
//                            }
//                        }}
                        progressView.dismiss();
                        showInputNicknameDialog();
                    }
                });
    }

    //finish game
    private void saveScore() {
        //stop timer
        timeSwapBuff += timeInMilliseconds;
        customHandler.removeCallbacks(updateTimerThread);

        User user = new DataPreferences(DashboardActivity.this).getUserSession();
        RequestParams params = new RequestParams();
        params.put("score[game_id]", game_id);
        params.put("score[score]", score);
        params.put("score[seconds]", total_time);
        RestClient.post("players/" + user.getUser_id() + "/scores.json", params,
                new JsonHttpResponseHandler() {
                    ProgressDialog progressView;

                    @Override
                    public void onStart() {
                        super.onStart();
                        progressView = new ProgressDialog(DashboardActivity.this);
                        progressView.setIndeterminate(true);
                        progressView.setCancelable(false);
                        progressView.setMessage("Mohon Tunggu, Score Anda sedang disimpan");
                        progressView.show();
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progressView.dismiss();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e("response", response.toString());

                        if (response.has("id")) {
                            Toast.makeText(getApplicationContext(), "Score Anda telah disimpan", Toast.LENGTH_SHORT).show();
                            setUpMap();
                            resetValue();
                        } else {
//                            Toast.makeText(getApplicationContext(), "Maaf, Score Anda gagal disimpan", Toast.LENGTH_SHORT).show();
                            saveScore();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Log.e("response", "error code : " + statusCode);
                    }
                });
    }

    private void updateDistanceInfo() {
//        Log.e("distance", "" + getDistance());
        if (game_id != 0) {
            if (nodeMarkerList.size() == 1) tvDistance.setText(Integer.toString(getDistance()));
            else tvDistance.setText("0");
        } else
            tvDistance.setText("0");
    }

    //
    private class MyLocationListener implements LocationListener {
        public void onLocationChanged(Location location) {
//            if (mMap.getMyLocation() != null)
//                userLocation = new LatLng(mMap.getMyLocation().getLatitude(), mMap.getMyLocation().getLongitude());
//            else
            gps = new GPSUtils(DashboardActivity.this);

            totalWalkDistance += getWalkDistance(location);

            userLocation = new LatLng(gps.getLatitude(), gps.getLongitude());

            if (userMarker != null) {
                userMarker.remove();
            }
            userMarker = mMap.addMarker(new MarkerOptions().position(
                    userLocation).title("Lokasiku").icon(BitmapDescriptorFactory
                    .fromResource(R.drawable.ic_marker_user)));

            if (game_id == 0) {
                setFocus = 0;
            } else {
                if (setFocus == 0) setFocus = 2;
                else if (setFocus == 2) setFocus = 1;
                else if (setFocus == 1) setFocus = 0;
                tvWalkDistance.setText(String.valueOf(totalWalkDistance));
            }

            settingFocus.performClick();
            updateDistanceInfo();
//            Log.i("Lokasi update", "Lokasi baru terdeteksi");
        }

        public void onStatusChanged(String s, int i, Bundle b) {
        }

        public void onProviderDisabled(String s) {
            Toast.makeText(DashboardActivity.this,
                    "Provider disabled, GPS off",
                    Toast.LENGTH_SHORT).show();
        }

        public void onProviderEnabled(String s) {
            Toast.makeText(DashboardActivity.this,
                    "Provider enabled, GPS on",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
