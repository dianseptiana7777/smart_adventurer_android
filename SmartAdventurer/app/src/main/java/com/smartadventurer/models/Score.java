package com.smartadventurer.models;


import com.smartadventurer.utils.JsonUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Score implements Serializable {
    private static final long serialVersionUID = -2615073922252547257L;
    private int id;
    private String name;
    private int score;
    private int seconds;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public static Score fromJsonObject(JSONObject json) {
        JSONObject jsonObject = json;
        if (jsonObject != null) {
            Score score = new Score();
            score.setId(JsonUtil.getInt(jsonObject, "id"));
            score.setName(JsonUtil.getString(jsonObject, "name"));
            score.setScore(JsonUtil.getInt(jsonObject, "score"));
            score.setSeconds(JsonUtil.getInt(jsonObject, "seconds"));
            return score;
        } else {
            return null;
        }
    }

    public static List<Score> getListFromJson(JSONObject json) {
        JSONArray jsonArray = JsonUtil.getJSONArray(json, "scores");
        List<Score> scoreList = new ArrayList<Score>();
        if (jsonArray != null) {
            int size = jsonArray.length();
            for (int i = 0; i < size; i++) {
                try {
                    Score score = fromJsonObject(jsonArray.getJSONObject(i));
                    if (score != null) {
                        scoreList.add(score);
                    }
                } catch (JSONException e) {
                }
            }
        }
        return scoreList;
    }
}
