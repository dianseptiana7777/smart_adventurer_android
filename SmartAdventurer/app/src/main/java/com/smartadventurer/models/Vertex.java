package com.smartadventurer.models;


import android.util.Log;

import com.smartadventurer.utils.JsonUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Vertex implements Serializable {
    private static final long serialVersionUID = 2552811314480890503L;
    final private String id;
    final private String name;

    public Vertex(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Vertex other = (Vertex) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return name;
    }

    public static Vertex fromJsonObject(JSONObject json) {
        JSONObject jsonObject = JsonUtil.getJSONObject(json, "vertex");
        if (jsonObject != null) {
            return new Vertex("Node_" + JsonUtil.getString(jsonObject, "id"), "Node_" + JsonUtil.getString(jsonObject, "id"));//node_type
        } else {
            return null;
        }
    }

    public static List<Vertex> getListFromJson(JSONObject json) {
        JSONArray jsonArray = JsonUtil.getJSONArray(json, "vertexes");
        List<Vertex> VertexList = new ArrayList<>();
        if (jsonArray != null) {
            int size = jsonArray.length();
            for (int i = 0; i < size; i++) {
                try {
                    Vertex vertex = fromJsonObject(jsonArray.getJSONObject(i));
                    if (vertex != null) {
                        Log.i("Titik", vertex.toString());
                        VertexList.add(vertex);
                    }
                } catch (JSONException e) {
                }
            }
        }
        return VertexList;
    }

    public static Vertex getVertexById(String vertexId, List<Vertex> vertexes){
        Vertex tmpVertex = null;
        for(Vertex vertex : vertexes){
            if(vertex.getId().equals(vertexId)){
                tmpVertex = vertex;
                break;
            }
        }
        return tmpVertex;
    }
}