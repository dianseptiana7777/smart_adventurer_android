package com.smartadventurer.models;

import java.io.Serializable;
import java.util.List;

public class Graph implements Serializable {
    private static final long serialVersionUID = 8963758711893299291L;
    private final List<Vertex> vertexes;
    private final List<Edge> edges;

    public Graph(List<Vertex> vertexes, List<Edge> edges) {
        this.vertexes = vertexes;
        this.edges = edges;
    }

    public List<Vertex> getVertexes() {
        return vertexes;
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public Edge getEdge(Vertex source, Vertex destination) {
        Edge resultEdge = null;
        for (Edge edge : edges) {
            if (edge.getSource().equals(source)
                    && edge.getDestination().equals(destination)) {
                resultEdge = edge;
                break;
            }
        }
        return resultEdge;
    }
}
