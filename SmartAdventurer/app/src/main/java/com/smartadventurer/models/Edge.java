package com.smartadventurer.models;

import com.smartadventurer.utils.JsonUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Edge implements Serializable {
    private static final long serialVersionUID = -7375738662345066506L;
    private String id;
    private Vertex source;
    private Vertex destination;
    private int source_id;
    private int destination_id;
    private int distance;
    private Double weight;

    public Edge() {

    }

    public Edge(String id, Vertex source, Vertex destination, Double weight) {
        this.id = id;
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }

    public static Edge fromJsonObject(JSONObject json) {
        JSONObject jsonObject = JsonUtil.getJSONObject(json, "edge");
        if (jsonObject != null) {
            Edge edge = new Edge();
//            edge.setId(JsonUtil.getString(jsonObject, "id"));
            edge.setSource_id(JsonUtil.getInt(jsonObject, "source"));
            edge.setDestination_id(JsonUtil.getInt(jsonObject, "destination"));
            edge.setId("Line_" + edge.getSource_id() + "_" + edge.getDestination_id());
            edge.setDistance(JsonUtil.getInt(jsonObject, "distance"));
            edge.setWeight(JsonUtil.getDouble(jsonObject, "total_weight"));
            return edge;
        } else {
            return null;
        }
    }

    public static List<Edge> getListFromJson(JSONObject json) {
        JSONArray jsonArray = JsonUtil.getJSONArray(json, "edges");
        List<Edge> edgeList = new ArrayList<>();
        if (jsonArray != null) {
            int size = jsonArray.length();
            for (int i = 0; i < size; i++) {
                try {
                    Edge Edge = fromJsonObject(jsonArray.getJSONObject(i));
                    if (Edge != null) {
                        edgeList.add(Edge);
                    }
                } catch (JSONException e) {
                }
            }
        }
        return edgeList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Vertex getSource() {
        return source;
    }

    public void setSource(Vertex source) {
        this.source = source;
    }

    public Vertex getDestination() {
        return destination;
    }

    public void setDestination(Vertex destination) {
        this.destination = destination;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return id + " " + source + " " + destination;
    }

    public int getSource_id() {
        return source_id;
    }

    public void setSource_id(int source_id) {
        this.source_id = source_id;
    }

    public int getDestination_id() {
        return destination_id;
    }

    public void setDestination_id(int destination_id) {
        this.destination_id = destination_id;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }
}