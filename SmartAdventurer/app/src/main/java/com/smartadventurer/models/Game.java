package com.smartadventurer.models;

import com.smartadventurer.utils.JsonUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Game implements Serializable {
    private static final long serialVersionUID = 4370592766983715618L;
    private int id;
    private String name;
    private List<Node> nodes;
    private List<Vertex> vertexes;
    private List<Edge> edges;

    public Game() {
    }

    public Game(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static Game fromJsonObject(JSONObject json) {
        JSONObject jsonObject = JsonUtil.getJSONObject(json, "game");
        if (jsonObject != null) {
            Game game = new Game();
            game.setId(JsonUtil.getInt(jsonObject, "id"));
            game.setName(JsonUtil.getString(jsonObject, "name"));
            game.setNodes(Node.getListFromJson(jsonObject));
            game.setVertexes(Vertex.getListFromJson(jsonObject));
            game.setEdges(Edge.getListFromJson(jsonObject));
            return game;
        } else {
            return null;
        }
    }

    public static List<Game> getListFromJson(JSONObject json) {
        JSONArray jsonArray = JsonUtil.getJSONArray(json, "games");
        List<Game> gameList = new ArrayList<>();
        if (jsonArray != null) {
            int size = jsonArray.length();
            for (int i = 0; i < size; i++) {
                try {
                    Game game = fromJsonObject(jsonArray.getJSONObject(i));
                    if (game != null) {
                        gameList.add(game);
                    }
                } catch (JSONException e) {
                }
            }
        }
        return gameList;
    }

    public static String[] getArrayGame(List<Game> gameList) {
        int size = gameList.size();
        String[] arrGame = new String[size];
        for (int i = 0; i < size; i++) {
            arrGame[i] = gameList.get(i).getName();
        }
        return arrGame;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge> edges) {
        this.edges = edges;
    }

    public List<Vertex> getVertexes() {
        return vertexes;
    }

    public void setVertexes(List<Vertex> vertexes) {
        this.vertexes = vertexes;
    }
}
