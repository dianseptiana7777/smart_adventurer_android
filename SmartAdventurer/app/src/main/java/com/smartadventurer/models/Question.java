package com.smartadventurer.models;

import com.smartadventurer.utils.JsonUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Question implements Serializable {
    private static final long serialVersionUID = 8766575591631835808L;
    private int id;
    private String question_text;
    private String answer_a;
    private String answer_b;
    private String answer_c;
    private String answer_d;
    private String answer_e;
    private String correct_answer;
    private String difficulty_level;
    private int difficulty_value;
    private int user_id;
    private String created_at;
    private String updated_at;

    public Question() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion_text() {
        return question_text;
    }

    public void setQuestion_text(String question_text) {
        this.question_text = question_text;
    }

    public String getAnswer_a() {
        return answer_a;
    }

    public void setAnswer_a(String answer_a) {
        this.answer_a = answer_a;
    }

    public String getAnswer_b() {
        return answer_b;
    }

    public void setAnswer_b(String answer_b) {
        this.answer_b = answer_b;
    }

    public String getAnswer_c() {
        return answer_c;
    }

    public void setAnswer_c(String answer_c) {
        this.answer_c = answer_c;
    }

    public String getAnswer_d() {
        return answer_d;
    }

    public void setAnswer_d(String answer_d) {
        this.answer_d = answer_d;
    }

    public String getAnswer_e() {
        return answer_e;
    }

    public void setAnswer_e(String answer_e) {
        this.answer_e = answer_e;
    }

    public String getCorrect_answer() {
        return correct_answer;
    }

    public void setCorrect_answer(String correct_answer) {
        this.correct_answer = correct_answer;
    }

    public String getDifficulty_level() {
        return difficulty_level;
    }

    public void setDifficulty_level(String difficulty_level) {
        this.difficulty_level = difficulty_level;
    }

    public int getDifficulty_value() {
        return difficulty_value;
    }

    public void setDifficulty_value(int difficulty_value) {
        this.difficulty_value = difficulty_value;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }


    public static Question fromJsonObject(JSONObject json) {
        JSONObject jsonObject = JsonUtil.getJSONObject(json, "question");
        if (jsonObject != null) {
            Question question = new Question();
            question.setId(JsonUtil.getInt(jsonObject, "id"));
            question.setQuestion_text(JsonUtil.getString(jsonObject, "question_text"));
            question.setAnswer_a(JsonUtil.getString(jsonObject, "answer_a"));
            question.setAnswer_b(JsonUtil.getString(jsonObject, "answer_b"));
            question.setAnswer_c(JsonUtil.getString(jsonObject, "answer_c"));
            question.setAnswer_d(JsonUtil.getString(jsonObject, "answer_d"));
            question.setAnswer_e(JsonUtil.getString(jsonObject, "answer_e"));
            question.setCorrect_answer(JsonUtil.getString(jsonObject, "correct_answer"));
            question.setDifficulty_level(JsonUtil.getString(jsonObject, "difficulty_level"));
            question.setDifficulty_value(JsonUtil.getInt(jsonObject, "difficulty_value"));
            question.setUser_id(JsonUtil.getInt(jsonObject, "user_id"));
            question.setCreated_at(JsonUtil.getString(jsonObject, "created_at"));
            question.setUpdated_at(JsonUtil.getString(jsonObject, "updated_at"));
            return question;
        } else {
            return null;
        }
    }

    public static List<Question> getListFromJson(JSONObject json) {
        JSONArray jsonArray = JsonUtil.getJSONArray(json, "questions");
        List<Question> questionList = new ArrayList<Question>();
        if (jsonArray != null) {
            int size = jsonArray.length();
            for (int i = 0; i < size; i++) {
                try {
                    Question Question = fromJsonObject(jsonArray.getJSONObject(i));
                    if (Question != null) {
                        questionList.add(Question);
                    }
                } catch (JSONException e) {
                }
            }
        }
        return questionList;
    }
}
