package com.smartadventurer.models;

import com.smartadventurer.utils.JsonUtil;

import org.json.JSONObject;

import java.io.Serializable;

public class User implements Serializable {

    private static final long serialVersionUID = 8226663735936800419L;
    private int user_id;
    private String nickname;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public static User fromJsonObject(JSONObject json) {
        JSONObject jsonObject = json;
        if (jsonObject != null) {
           User user = new User();
            user.setUser_id(JsonUtil.getInt(jsonObject, "id"));
            user.setNickname(JsonUtil.getString(jsonObject, "nickname"));
            return user;
        } else {
            return null;
        }
    }
}
