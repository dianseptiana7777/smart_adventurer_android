package com.smartadventurer.models;

import com.smartadventurer.utils.JsonUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Node implements Serializable {
    private static final long serialVersionUID = -3172097175582404607L;
    private int id;
    private String node_type;
    private double latitude;
    private double longitude;
    private double distance;
    private String difficulty_level;
    private int difficulty_value;
    private List<Node> connected_nodes;

    public Node() {
    }

    public static Node fromJsonObject(JSONObject json) {
        JSONObject jsonObject = JsonUtil.getJSONObject(json, "node");
        if (jsonObject != null) {
            Node node = new Node();
            node.setId(JsonUtil.getInt(jsonObject, "id"));
            node.setNode_type(JsonUtil.getString(jsonObject, "node_type"));
            node.setLatitude(JsonUtil.getDouble(jsonObject, "latitude"));
            node.setLongitude(JsonUtil.getDouble(jsonObject, "longitude"));
            node.setDistance(JsonUtil.getDouble(jsonObject, "distance"));
            node.setDifficulty_level(JsonUtil.getString(jsonObject, "difficulty_level"));
            node.setDifficulty_value(JsonUtil.getInt(jsonObject, "difficulty_value"));
            node.setConnected_nodes(getListFromJson(jsonObject, "connected_nodes"));
            return node;
        } else {
            return null;
        }
    }

    public static List<Node> getListFromJson(JSONObject json) {
        JSONArray jsonArray = JsonUtil.getJSONArray(json, "nodes");
        List<Node> nodeList = new ArrayList<Node>();
        if (jsonArray != null) {
            int size = jsonArray.length();
            for (int i = 0; i < size; i++) {
                try {
                    Node node = fromJsonObject(jsonArray.getJSONObject(i));
                    if (node != null) {
                        nodeList.add(node);
                    }
                } catch (JSONException e) {
                }
            }
        }
        return nodeList;
    }

    public static List<Node> getListFromJson(JSONObject json, String key) {
        JSONArray jsonArray = JsonUtil.getJSONArray(json, key);
        List<Node> nodeList = new ArrayList<Node>();
        if (jsonArray != null) {
            int size = jsonArray.length();
            for (int i = 0; i < size; i++) {
                try {
                    Node node = fromJsonObject(jsonArray.getJSONObject(i));
                    if (node != null) {
                        nodeList.add(node);
                    }
                } catch (JSONException e) {
                }
            }
        }
        return nodeList;
    }

    public static Node getStartNode(List<Node> nodeList) {
        Node startNode = null;
        int size = nodeList.size();
        for (int i = 0; i < size; i++) {
            Node node = nodeList.get(i);
            if (node.getNode_type().equalsIgnoreCase("start")) {
                startNode = node;
            }
        }
        return startNode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNode_type() {
        return node_type;
    }

    public void setNode_type(String node_type) {
        this.node_type = node_type;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getDifficulty_level() {
        return difficulty_level;
    }

    public void setDifficulty_level(String difficulty_level) {
        this.difficulty_level = difficulty_level;
    }

    public int getDifficulty_value() {
        return difficulty_value;
    }

    public void setDifficulty_value(int difficulty_value) {
        this.difficulty_value = difficulty_value;
    }

    public List<Node> getConnected_nodes() {
        return connected_nodes;
    }

    public void setConnected_nodes(List<Node> connected_nodes) {
        this.connected_nodes = connected_nodes;
    }

    @Override
    public String toString() {
        return "id : " + getId() +
                "\nnode_type :" + getNode_type() +
                "\nlatitude :" + getLatitude() +
                "\nlongitude :" + getLongitude() +
                "\ndistance :" + getDistance() +
                "\ndifficulty_level :" + getDifficulty_level() +
                "\ndifficulty_value :" + getDifficulty_value() +
                "\nconnected_nodes :" + getConnected_nodes();
    }

}
