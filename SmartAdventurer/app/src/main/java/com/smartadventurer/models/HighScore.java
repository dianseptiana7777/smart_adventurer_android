package com.smartadventurer.models;


import com.smartadventurer.utils.JsonUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HighScore implements Serializable {
    private static final long serialVersionUID = 9131263575243262262L;
    private int id;
    private String nickname;
    private int total_scores;
    private int total_seconds;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getTotal_scores() {
        return total_scores;
    }

    public void setTotal_scores(int total_scores) {
        this.total_scores = total_scores;
    }

    public int getTotal_seconds() {
        return total_seconds;
    }

    public void setTotal_seconds(int total_seconds) {
        this.total_seconds = total_seconds;
    }

    public static HighScore fromJsonObject(JSONObject json) {
        JSONObject jsonObject = json;
        if (jsonObject != null) {
            HighScore highScore = new HighScore();
            highScore.setId(JsonUtil.getInt(jsonObject, "id"));
            highScore.setNickname(JsonUtil.getString(jsonObject, "nickname"));
            highScore.setTotal_scores(JsonUtil.getInt(jsonObject, "total_scores"));
            highScore.setTotal_seconds(JsonUtil.getInt(jsonObject, "total_seconds"));
            return highScore;
        } else {
            return null;
        }
    }

    public static List<HighScore> getListFromJson(JSONObject json) {
        JSONArray jsonArray = JsonUtil.getJSONArray(json, "high_scores");
        List<HighScore> highScoreList = new ArrayList<HighScore>();
        if (jsonArray != null) {
            int size = jsonArray.length();
            for (int i = 0; i < size; i++) {
                try {
                    HighScore highScore = fromJsonObject(jsonArray.getJSONObject(i));
                    if (highScore != null) {
                        highScoreList.add(highScore);
                    }
                } catch (JSONException e) {
                }
            }
        }
        return highScoreList;
    }
}

