package com.smartadventurer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;


public class SplashScreenActivity extends AppCompatActivity {
    protected boolean _aktif = true;
    protected int _waktuSplash = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_splash_screen);
        Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    super.run();
                    while (_aktif && (waited < _waktuSplash)) {
                        sleep(100);
                        if (_aktif) {
                            waited += 100;
                        }
                    }
                } catch (InterruptedException e) {
                } finally {
                    startActivity(new Intent(SplashScreenActivity.this,
                            DashboardActivity.class));
                    finish();
                }
            }
        };
        splashTread.start();
    }
}
