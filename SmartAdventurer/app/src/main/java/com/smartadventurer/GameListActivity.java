package com.smartadventurer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.smartadventurer.adapter.GameAdapter;
import com.smartadventurer.models.Game;
import com.smartadventurer.rest.RestClient;

import org.apache.http.Header;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GameListActivity extends AppCompatActivity {
    private ListView listView;
    private List<Game> gameList;
    private ProgressBar pbLoader;
    private EditText etSearch;
    private GameAdapter gameAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_game_list);
        etSearch = (EditText) findViewById(R.id.etSearch);
        listView = (ListView) findViewById(R.id.listView);
        listView.setVisibility(View.GONE);
        pbLoader = (ProgressBar) findViewById(R.id.pbLoader);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder alert = new AlertDialog.Builder(
                        GameListActivity.this);
                alert.setTitle("Konfirmasi Pilih Game")
                        .setMessage("Apakah anda ingin memainkan game ini ?")
                        .setCancelable(false)
                        .setPositiveButton("Ya",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.cancel();
                                        Intent returnIntent = new Intent();
                                        returnIntent.putExtra(Constant.KEY_GAME_ID, gameList.get(position).getId());
                                        setResult(RESULT_OK, returnIntent);
                                        finish();
                                    }
                                })
                        .setNegativeButton("Tidak",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.cancel();
                                    }
                                }).create().show();
            }
        });

        getGameList();

        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (count < before) {

                    gameAdapter.resetData();
                }
                gameAdapter.getFilter().filter(s.toString());

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getGameList() {
        RestClient.get("games.json", null,
                new JsonHttpResponseHandler() {
                    @Override
                    public void onStart() {
                        super.onStart();
                        pbLoader.setVisibility(View.VISIBLE);
                        if (gameList == null) {
                            gameList = new ArrayList<>();
                        }
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        pbLoader.setVisibility(View.GONE);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.i("response", response.toString());
                        gameList = Game.getListFromJson(response);
                        if (!gameList.isEmpty()) {
                            gameAdapter = new GameAdapter(GameListActivity.this, gameList);
                            listView.setAdapter(gameAdapter);
                            listView.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Log.e("response", "error code : " + statusCode);
                    }
                });
    }

}
