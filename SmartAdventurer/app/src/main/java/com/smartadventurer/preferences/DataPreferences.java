package com.smartadventurer.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.smartadventurer.models.User;

public class DataPreferences {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "smart_adventurer";

    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_NICKNAME = "nickname";

    public DataPreferences(Context context) {
        this._context = context;
        this.pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        this.editor = pref.edit();
    }

    public void createUserSession(User user) {
        editor.putInt(KEY_USER_ID, user.getUser_id());
        editor.putString(KEY_NICKNAME, user.getNickname());
        editor.commit();
    }

    public User getUserSession() {
        User userSession = new User();
        userSession.setUser_id(pref.getInt(KEY_USER_ID, 0));
        userSession.setNickname(pref.getString(KEY_NICKNAME, ""));
        return userSession;
    }
}

